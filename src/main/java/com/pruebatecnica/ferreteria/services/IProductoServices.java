package com.pruebatecnica.ferreteria.services;


import com.pruebatecnica.ferreteria.model.Producto;

public interface IProductoServices {

	Producto buscarPorId(Long idProducto);
	Producto actualizarPorId(Long idProdcuto,Producto actualizar);
	Producto actualizarPorCodigo(String codigo,Producto actualizar);
	Producto buscarPorCodigo(String codigo);
}
