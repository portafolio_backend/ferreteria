package com.pruebatecnica.ferreteria.services;

import com.pruebatecnica.ferreteria.dto.OrdenDtoDos;
import com.pruebatecnica.ferreteria.model.Orden;

public interface IOrdenServices {


	Orden crearOrdenDos(OrdenDtoDos crear);
	Orden buscarPorId(Long idOrden);

}