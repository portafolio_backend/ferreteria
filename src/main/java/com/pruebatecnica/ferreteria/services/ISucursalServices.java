package com.pruebatecnica.ferreteria.services;

import java.util.List;

import com.pruebatecnica.ferreteria.model.Sucursal;

public interface ISucursalServices {
	
	List<Sucursal> listar();
	Sucursal buscarPorId(Long idSucursal);
	Sucursal buscarPorNombre(String nombre);

}
