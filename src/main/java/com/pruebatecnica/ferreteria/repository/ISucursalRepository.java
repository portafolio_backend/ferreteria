package com.pruebatecnica.ferreteria.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pruebatecnica.ferreteria.model.Sucursal;

@Repository
public interface ISucursalRepository extends CrudRepository<Sucursal, Long>{

	@Query(value = "SELECT s FROM Sucursal s WHERE s.nombre = ?1")
	Sucursal findByNombre(@Param("nombre") String nombre);

}
