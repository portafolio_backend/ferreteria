package com.pruebatecnica.ferreteria.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.pruebatecnica.ferreteria.model.Orden;

@Repository
public interface IOrdenRepository extends CrudRepository<Orden, Long>{

}
