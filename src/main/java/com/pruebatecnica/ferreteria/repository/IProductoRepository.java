package com.pruebatecnica.ferreteria.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.pruebatecnica.ferreteria.model.Producto;

@Repository
public interface IProductoRepository extends CrudRepository<Producto, Long>{

	@Query(value = "SELECT p FROM Producto p WHERE p.codigo =?1")
	Producto findByCodigo(@Param("codigo") String codigo);

}
