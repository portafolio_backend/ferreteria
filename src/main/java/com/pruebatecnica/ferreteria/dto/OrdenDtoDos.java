package com.pruebatecnica.ferreteria.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class OrdenDtoDos implements Serializable{

	private String sucursalId;
	private List<Long> productoIds;
	
	private static final long serialVersionUID = -8643841999995158259L;

	
	

}