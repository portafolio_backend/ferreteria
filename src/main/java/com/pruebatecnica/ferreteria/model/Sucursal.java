package com.pruebatecnica.ferreteria.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "sucursales")
@Data
public class Sucursal implements Serializable{



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_sucursal")
	private Long idSucursal;
	
	@Column(name = "nombre")
	private String nombre;

	
	private static final long serialVersionUID = 2306470036725947045L;


}