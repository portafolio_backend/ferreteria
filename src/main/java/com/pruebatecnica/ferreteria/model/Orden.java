package com.pruebatecnica.ferreteria.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import lombok.Data;

@Entity
@Table(name = "ordenes")
@Data
public class Orden implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_orden")
	private Long idOrden;
	
	@JoinColumn(name = "sucursal_id",referencedColumnName = "id_sucursal")
	private Sucursal sucursalId;
	
	@OneToMany(mappedBy = "orden")
	private List<Producto> productoId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "fecha")
	private Date fehca;
	
	@Column(name = "total")
	private Double total;
	
	
	@PrePersist
	private void fecha () {
		this.fehca = new Date();
	}

	private static final long serialVersionUID = 851391065223611823L;
	


}
