package com.pruebatecnica.ferreteria.servicesImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.pruebatecnica.ferreteria.dto.OrdenDtoDos;
import com.pruebatecnica.ferreteria.model.Orden;
import com.pruebatecnica.ferreteria.model.Producto;
import com.pruebatecnica.ferreteria.repository.IOrdenRepository;
import com.pruebatecnica.ferreteria.services.IOrdenServices;
import com.pruebatecnica.ferreteria.services.IProductoServices;
import com.pruebatecnica.ferreteria.services.ISucursalServices;

@Service
public class OrdenServicesImpl implements IOrdenServices {

	private IOrdenRepository ordenRepository;

	public OrdenServicesImpl(IOrdenRepository ordenRepository) {
		this.ordenRepository = ordenRepository;
	}

	@Autowired
	private ISucursalServices sucursalServices;

	@Autowired
	private IProductoServices productoServices;


	
	@Override
	public Orden crearOrdenDos(OrdenDtoDos crear) {
	    Orden orden = new Orden();
	    try {
	        orden.getIdOrden();
	        orden.setSucursalId(sucursalServices.buscarPorNombre(crear.getSucursalId()));

	        List<Producto> productos = new ArrayList<>();
	        Double total = 0.0;

	        for (Long productoId : crear.getProductoIds()) {
	            Producto producto = productoServices.buscarPorId(productoId);
	            productos.add(producto);
	            total += producto.getPrecio();
	        }

	        orden.setProductoId((List<Producto>) productos); 
	        orden.getFehca();
	        orden.setTotal(total);

	        ordenRepository.save(orden);
	        
	    } catch (DataAccessException e) {
	        e.getMostSpecificCause().getCause();
	        throw new RuntimeException("Error al crear la orden");
	    }

	    return orden;
	}
	
	
	@Override
	public Orden buscarPorId(Long idOrden) {
		Optional<Orden> ordenOptional = ordenRepository.findById(idOrden);
		if (ordenOptional.isPresent()) {
			return ordenOptional.get();
		} else {
			throw new RuntimeException("No se encontró la orden con ID: " + idOrden);
		}
	}


}
