package com.pruebatecnica.ferreteria.servicesImpl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.pruebatecnica.ferreteria.model.Sucursal;
import com.pruebatecnica.ferreteria.repository.ISucursalRepository;
import com.pruebatecnica.ferreteria.services.ISucursalServices;

@Service
public class SucursalServicesImpl implements ISucursalServices{
	
	
	private ISucursalRepository sucursalRepository;
	
	public SucursalServicesImpl(ISucursalRepository sucursalRepository) {
		this.sucursalRepository = sucursalRepository;
	}

	@Override
	public Sucursal buscarPorId(Long idSucursal) {
		return sucursalRepository.findById(idSucursal).orElse(null);
	}

	@Override
	public Sucursal buscarPorNombre(String nombre) {
		return sucursalRepository.findByNombre(nombre);
	}

	@Override
	public List<Sucursal> listar() {
		return (List<Sucursal>) sucursalRepository.findAll();
	}

}
