package com.pruebatecnica.ferreteria.servicesImpl;

import java.util.Optional;

import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.pruebatecnica.ferreteria.model.Producto;
import com.pruebatecnica.ferreteria.repository.IProductoRepository;
import com.pruebatecnica.ferreteria.services.IProductoServices;


@Service
public class ProductoServicesImpl implements IProductoServices{

	private IProductoRepository productoRepository;
	
	public ProductoServicesImpl(IProductoRepository productoRepository) {
		this.productoRepository = productoRepository;
	}

	@Override
	public Producto buscarPorId(Long idProducto) {
		Optional<Producto> productoOptional = productoRepository.findById(idProducto);
	    if (productoOptional.isPresent()) {
	        return productoOptional.get();
	    } else {
	        throw new RuntimeException("No se encontró el producto con ID: " + idProducto);
	    }
	}

	@Override
	public Producto buscarPorCodigo(String codigo) {
		Optional<Producto> productoOptional = Optional.ofNullable(productoRepository.findByCodigo(codigo));
	    if (productoOptional.isPresent()) {
	        return productoOptional.get();
	    } else {
	        throw new RuntimeException("No se encontró el producto con el codigo : " + codigo);
	    }
	}

	

	@Override
	public Producto actualizarPorId(Long idProdcuto, Producto actualizar) {
		Producto producto = buscarPorId(idProdcuto); 	
		try {
			producto.setPrecio(actualizar.getPrecio());
			productoRepository.save(producto);
		}catch (DataAccessException e) {
			
			e.getMostSpecificCause().getMessage();
		}
		return producto;
	}
	
	@Override
	public Producto actualizarPorCodigo(String codigo, Producto actualizar) {
		Producto producto = buscarPorCodigo(codigo); 	
		try {
			producto.setPrecio(actualizar.getPrecio());
			productoRepository.save(producto);
		}catch (DataAccessException e) {
			e.getMostSpecificCause().getMessage();
		}
		return producto;
	}
	
}
