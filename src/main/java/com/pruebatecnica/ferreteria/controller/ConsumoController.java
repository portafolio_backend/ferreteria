package com.pruebatecnica.ferreteria.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.ValidationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.pruebatecnica.ferreteria.dto.OrdenDtoDos;
import com.pruebatecnica.ferreteria.model.Orden;
import com.pruebatecnica.ferreteria.model.Producto;
import com.pruebatecnica.ferreteria.services.IOrdenServices;
import com.pruebatecnica.ferreteria.services.IProductoServices;
import com.pruebatecnica.ferreteria.services.ISucursalServices;

@RestController
@RequestMapping("/consumo")
public class ConsumoController {

	@Autowired
	ISucursalServices sucursalServices;
	
	@Autowired
	IProductoServices productoServices;
	
	@Autowired
	IOrdenServices ordenServices;
	

	@PutMapping("/updateByCodigoProducto/{codigo}")
	public ResponseEntity<?> actualizarProducto(@PathVariable("codigo")String codigo,@RequestBody Producto actualizar)  {
		Producto producto = null;
		Map<String,Object> response = new HashMap<>();
		try {
			producto = productoServices.actualizarPorCodigo(codigo, actualizar);
		}catch (DataAccessException e) {
			response.put("mensaje", "error al realizar la busqueda en la base de datos ");
			response.put("error", e.getMessage().concat(" : ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}if(producto == null) {
			response.put("mensaje", "el producto con el codigo ".concat(codigo.toString().concat(" no exixste en la base de dats")));
			return new ResponseEntity<Map<String,Object>>(response, HttpStatus.NOT_FOUND);
		} return new ResponseEntity<Producto> (producto,HttpStatus.OK);
	}
	
	@PutMapping("/updateByIdProducto/{idPoructo}")
	public ResponseEntity<?> actualizarProducto(@PathVariable("idPoructo")Long idPoructo,@RequestBody Producto actualizar) {
		Producto producto = null;
		Map<String,Object> response = new HashMap<>();
		try {
			producto = productoServices.actualizarPorId(idPoructo, actualizar);
		}catch (DataAccessException e) {
			response.put("mensaje", "error al realizar la busqueda en la base de datos ");
			response.put("error", e.getMessage().concat(" : ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}if(producto == null) {
			response.put("mensaje", "el producto con el ID ".concat(idPoructo.toString().concat(" no exixste en la base de dats")));
			return new ResponseEntity<Map<String,Object>>(response, HttpStatus.NOT_FOUND);
		} return new ResponseEntity<Producto> (producto,HttpStatus.OK);
	}

	@JsonIgnore
	@PostMapping("/crearOrdendos")
	public ResponseEntity<?> crearOrdendos(@RequestBody OrdenDtoDos crear ){
		Orden orden = null;
		Map<String,Object> response = new HashMap<>();
		try {
			orden = ordenServices.crearOrdenDos(crear);
		} catch (ValidationException e) {
			response.put("mensaje", "eror al registrar al Orden ");
			return new ResponseEntity<Map<String,Object>> (response, HttpStatus.INTERNAL_SERVER_ERROR);
			} return new ResponseEntity<Orden> (orden, HttpStatus.OK);
	}
	
	@GetMapping("findByIdOrden/{idOrden}")
	public Orden buscarOrden(@PathVariable("idOrden")Long idOrden) {
		return ordenServices.buscarPorId(idOrden);
	}


}
